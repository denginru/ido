package Lab2;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

class Edge implements Comparable
{
    int v, w, weight;

    public Edge(int v, int w, int weight) {
        this.v = v;
        this.w = w;
        this.weight = weight;
    }

    public int getV() {
        return v;
    }

    public void setV(int v) {
        this.v = v;
    }

    public int getW() {
        return w;
    }

    public void setW(int w) {
        this.w = w;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public int compareTo(Object obj) {
        Edge o = (Edge)obj;
        return Integer.compare(this.weight, o.weight);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Edge edge = (Edge) o;

        if (v != edge.v) return false;
        return w == edge.w;

    }

    @Override
    public int hashCode() {
        int result = v;
        result = 31 * result + w;
        return result;
    }

    @Override
    public String toString() {
        return "{" + v +", " + w + '}' + " - " + this.weight;
    }
}

class Neighbor
{
    private int vertixNumber, weight;

    public Neighbor(int vertixNumber, int weight) {
        this.vertixNumber = vertixNumber;
        this.weight = weight;
    }

    public int getVertixNumber() {
        return vertixNumber;
    }

    public void setVertixNumber(int vertixNumber) {
        this.vertixNumber = vertixNumber;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}

public class Lab2 {
    private static ArrayList<ArrayList<Neighbor>> graph;    //граф представленный списками смежностей (список списков)
    private static int countOfVertex;                      //количество вершин
    private static Scanner in;
    private static int name[], next[], size[];
    private static TreeSet<Integer> ostov;

    private static void merge(int v, int w)
    {
        int p = name[v], q=name[w];
        name[w]=p;
        int u = next[w];
        while (name[u] != p)
        {
            name[u]=p;
            u=next[u];
        }
        size[p] += size[q];
        int x=next[v], y=next[w];
        next[v]=y; next[w]=x;
    }

    public static ArrayList<Edge> methodBK()
    {
        //TreeSet<Edge> queue = new TreeSet<Edge>();
        ArrayList<Edge> Q = new ArrayList<Edge>();

        //Составляем очередь рёбер
        //цикл по всем вершинам
        for (int i=1; i<=countOfVertex; i++)
        {
            //по всем соседям i-ой вершины
            for (int j=0; j<graph.get(i).size(); j++)
            {
                int neighborVertixNumber = graph.get(i).get(j).getVertixNumber();
                if (neighborVertixNumber >= i)
                    Q.add(new Edge(i, neighborVertixNumber, graph.get(i).get(j).getWeight()));
            }
        }
        Q.sort(new Comparator<Edge>() {
            @Override
            public int compare(Edge o1, Edge o2) {
                return Integer.compare(o1.weight, o2.weight);
            }
        });
        for (int v=1; v<=countOfVertex; v++)
        {
            name[v]=v;
            next[v]=v;
            size[v]=1;
        }

        ArrayList<Edge> T = new ArrayList<Edge>();
        int i=0;
        while (T.size() != countOfVertex - 1)
        {
            Edge edg = Q.get(i);
            int p = name[edg.v], q = name[edg.w];
            if (p != q)
            {
                if (size[p] > size[q])
                    merge(edg.w, edg.v);
                else
                    merge(edg.v, edg.w);
                T.add(edg);
            }
            i++;
        }
        return T;
    }

    public static void main(String[] args) throws IOException
    {
        in = new Scanner(new File("in.txt"));
        ostov = new TreeSet<>();
        graph = new ArrayList<>();
        graph.add(new ArrayList<>());   //т.к. нумерация вершин в задаче с 1, а в Java с 0, то нулевая ячейка пустая должна быть

        countOfVertex = in.nextInt();

        name = new int[countOfVertex+1];
        next = new int[countOfVertex+1];
        size = new int[countOfVertex+1];

        //чтение графа
        in.nextLine();
        for (int i=0; i<countOfVertex; i++)
        {
            String[] s = in.nextLine().trim().split(" ");
            ArrayList<Neighbor> neighbours = new ArrayList<>();
            for (int j=0; j<s.length-1; j+=2)
                neighbours.add(new Neighbor(new Integer(s[j]), new Integer(s[j+1])));
            graph.add(neighbours);
        }

        ArrayList<ArrayList<Neighbor>> ostov;
        ostov = new ArrayList<>(); ostov.add(new ArrayList<Neighbor>());
        ArrayList<Edge> ostovEdges = methodBK();

        //сортировка ребер по номеру вершин
        ostovEdges.sort(new Comparator<Edge>() {
            @Override
            public int compare(Edge o1, Edge o2) {
                if (o1.v == o2.v)
                    return Integer.compare(o1.w, o2.w);
                else
                    return Integer.compare(o1.v, o2.v);
            }
        });

        //инструмент для вывода результатов
        PrintWriter out = new PrintWriter(new File("out.txt"));

        //формируем списки смежностей остова
        ArrayList<Integer> ostovT[] = new ArrayList[countOfVertex+1];
        for (int i=0; i<=countOfVertex; i++)
            ostovT[i] = new ArrayList<>();
        int totalMinWeight = 0;     //вес остова
        for (int i=0; i<ostovEdges.size(); i++) {
            ostovT[ostovEdges.get(i).v].add(ostovEdges.get(i).w);   //для вершины v добавляем соседа w
            ostovT[ostovEdges.get(i).w].add(ostovEdges.get(i).v);   //для вершины w добавляем соседа v
            totalMinWeight += ostovEdges.get(i).getWeight();
        }

        //выводим остов как списки смежностей
        for (int i=1; i<=countOfVertex; i++)
        {
            for (int j=0; j<ostovT[i].size(); j++)
                out.print(ostovT[i].get(j) + " ");
            out.println(0);
        }
        out.println(totalMinWeight);
        out.flush();
        out.println();
    }
}
