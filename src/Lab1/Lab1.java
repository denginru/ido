package Lab1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by dru on 09.11.16.
 */
public class Lab1 {
    private static ArrayList<ArrayList<Integer>> graph;
    private static int vertixCount; //количество вершин
    private static boolean[] groups;    //i-ый элемент массива равен false если i-ая вершина пренадлежит 0-ой доли и true, если 1-ой
    private static boolean[] visited;   //i-ый элемент массива равен false, если i-ая вершина ещё не была посещена, и true если была
    private static boolean succes = true;   //результат обхода

    private static void DFS(int v, boolean group)
    {
        if (!succes) return;    //если на каком-то шаге произошёл не успех, идти дальше бессмысленно. Выходим
        groups[v]=group;    //если всё хорошо, то определяем вершину в долю
        visited[v] = true;  //помечаем её посещённой
        ArrayList<Integer> neighbors = graph.get(v);    //соседи v
        for (Object vertix:neighbors)   //для всех соседий v
        {
            //если текущий сосед уже был посещен и
            // пренадлежит той же группе, что и текущая вершина v,
            if (visited[(Integer)vertix] && groups[(Integer)vertix]==group)
            {
                succes = false;     //то граф не двудольный
                return;             // выходим
            }
            if (!visited[(Integer)vertix])      //если сосед не посещён,
                DFS((Integer)vertix, !group);   // посещаем его
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        ArrayList<Integer> neighborsOfVertix;   //соседи текущей вершины
        graph = new ArrayList<>(); graph.add(new ArrayList<>());    //сам граф, представленный списками смежности (список из списков)
        Scanner in = new Scanner(new File("in.txt"));   //инструмент для чтения
        PrintWriter out = new PrintWriter(new File("out.txt")); //инструмент для записи

        vertixCount = in.nextInt();     //читаем количество вершин
        //задаём размеры массивов(они на 1 больше, чем количество вершин,
        // потому что нумерация в java начинается с 0, а в исходных данных с 1)
        groups = new boolean[vertixCount+1];
        visited = new boolean[vertixCount+1];

        //в начале ни одна вершина не посещена
        for (int i=0; i<vertixCount+1; i++)
            visited[i] = false;

        for (int i=1; i<=vertixCount; i++)   //i - номер текущей вершины (а также номер строки)
        {
            neighborsOfVertix = new ArrayList<>();  //список соседей i-ой вершины
            int currentVertix;      //текущий сосед (очередная вершина, записанная в строке)
            while (true)
            {
                currentVertix = in.nextInt();   // читаем очередного соседа
                if (currentVertix != 0)         // если это не знак конца ввода, значит это номер вершины, которая является соседом
                    neighborsOfVertix.add(currentVertix);   //Добавляем её в список соседей
                else    //Если прочитали 0, то это признак конца ввода
                {
                    graph.add(neighborsOfVertix);   //добавляем сформированный список соседей в граф (список списков)
                    break;                          //Прерываем чтение текущей строки
                }
            }
        }

        DFS(1, false);  //начинаем с 1-ой вершины размещать вершины по долям
        if (!succes)    //Если не получилось распределить по долям
            out.println("N");   //выводим N
        else    //Если всё хорошо, то выводим доли в формате, описанном в задании
        {
            out.println("Y");
            for (int i=1; i<=vertixCount; i++)
                if (groups[i]==false)
                    out.print(i+" ");
            out.println(0);
            for (int i=1; i<=vertixCount; i++)
                if (groups[i]==true)
                    out.print(i+" ");
        }
        //закрываем выходной файл
        out.flush();
        out.close();
        //закрываем входной файл
        in.close();
    }
}
