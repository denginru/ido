package Lab3;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;
import java.util.TreeSet;

class Neighbor
{
    private int vertixNumber, weight;

    public Neighbor(int vertixNumber, int weight) {
        this.vertixNumber = vertixNumber;
        this.weight = weight;
    }

    public int getVertixNumber() {
        return vertixNumber;
    }

    public void setVertixNumber(int vertixNumber) {
        this.vertixNumber = vertixNumber;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}

public class Lab3 {
    private static ArrayList<ArrayList<Neighbor>> graph;    //граф представленный списками смежностей (список списков)
    private static int countOfVertex;                      //количество вершин
    private static Scanner in;
    private static PrintWriter out;
    private static int s, t;
    private static int inf=1000000000;
    private static TreeSet<Integer> T;
    private static TreeSet<Integer> S;
    private static int D[], parent[];

    public static int MIN()
    {
        int _min=inf, ans=-1;
        for (Integer v:T)
            if (D[v]<_min && D[v]>-inf)
            {
                _min = D[v];
                ans=v;
            }
        return ans;
    }

    private static int getWeight(int v, int w)
    {
        int res = inf;
        for (int i=0; i<graph.get(v).size(); i++)
            if (graph.get(v).get(i).getVertixNumber() == w)
                res=graph.get(v).get(i).getWeight();
        return res;
    }

    public static Stack<Integer> modyfiedDeykstra()
    {
        Stack<Integer> st = new Stack<>();
        S.add(s);   //S={s}

        // T = V / {s}
        for (int i=1; i<=countOfVertex; i++)
            if (i!=s)
                T.add(i);

        D[s]=-inf;
        for (Integer v: T)
        {
            D[v]=getWeight(s,v);
            parent[v]=s;
        }

        while (!S.contains((Integer)t))
        {
            int w = MIN();
            if (w != -1) {
                T.remove(w);
                S.add(w);
                for (Integer v : T) {
                    int a = getWeight(w, v);
                    if (D[v] > Math.max(D[w], a)) {
                        D[v] = Math.max(D[w], a);
                        parent[v] = w;
                    }
                }
            }
            else
                break;
        }
        int d = D[t];
        if (d < inf)
        {
            st.push(t);
            while (st.peek() != s)
                st.push(parent[st.peek()]);
        }
        return st;
    }



    public static void main(String[] agrs) throws IOException
    {
        in = new Scanner(new File("in.txt"));
        out = new PrintWriter(new File("out.txt"));

        graph = new ArrayList<>();
        graph.add(new ArrayList<>());   //т.к. нумерация вершин в задаче с 1, а в Java с 0, то нулевая ячейка пустая должна быть

        countOfVertex = in.nextInt();in.nextLine();
        D = new int[countOfVertex+1];
        parent = new int[countOfVertex+1];
        T = new TreeSet<>();
        S = new TreeSet<>();

        for (int i=0; i<countOfVertex; i++)
        {
            String[] s = in.nextLine().trim().split(" ");
            ArrayList<Neighbor> neighbours = new ArrayList<>();
            for (int j=0; j<s.length-1; j+=2)
                neighbours.add(new Neighbor(new Integer(s[j]), new Integer(s[j+1])));
            graph.add(neighbours);
        }

        s=in.nextInt(); t=in.nextInt();

        Stack<Integer> res = modyfiedDeykstra();
        if (!res.empty())
        {
            int max_weight = 0, cur_weight;
            int prev_v=-1;
            out.println("Y");
            while (!res.isEmpty()) {
                if (res.peek() != s)
                {
                    cur_weight=getWeight(prev_v, res.peek());
                    if (cur_weight > max_weight)
                        max_weight = cur_weight;
                }
                prev_v=res.peek();
                out.print(res.pop() + " ");
            }
            out.println();
            out.print(max_weight);
        }
        else
            out.println("N");
        out.flush();
        out.close();
    }
}
